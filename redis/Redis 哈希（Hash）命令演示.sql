
127.0.0.1:6379[2]>  hset myHash id 1
(integer) 1
127.0.0.1:6379[2]> hgetall myHash
1) "id"
2) "1"
127.0.0.1:6379[2]> hset myHash id 2
(integer) 0
127.0.0.1:6379[2]> hgetall myHash
1) "id"
2) "2"
127.0.0.1:6379[2]> hgetall myHash
1) "id"
2) "2"
127.0.0.1:6379[2]> hmset myHash id 3 name zhangSan
OK
127.0.0.1:6379[2]> hgetall myHash
1) "id"
2) "3"
3) "name"
4) "zhangSan"
127.0.0.1:6379[2]> hget myHash name
"zhangSan"
127.0.0.1:6379[2]> hget myHash name1
(nil)
127.0.0.1:6379[2]> hget myHash1 name
(nil)
127.0.0.1:6379[2]> hmget myHash id name age
1) "3"
2) "zhangSan"
3) (nil)
127.0.0.1:6379[2]> hgetall myHash
1) "id"
2) "3"
3) "name"
4) "zhangSan"
127.0.0.1:6379[2]> hgetall myHash1
(empty list or set)
127.0.0.1:6379[2]> hgetall user
(empty list or set)
127.0.0.1:6379[2]> hexists  myHash id
(integer) 1
127.0.0.1:6379[2]> hexists  myHash id22
(integer) 0
127.0.0.1:6379[2]> hlen myHash
(integer) 2
127.0.0.1:6379[2]> hkeys user
(empty list or set)
127.0.0.1:6379[2]> hkeys myHash
1) "id"
2) "name"
127.0.0.1:6379[2]> hvals  myHash
1) "3"
2) "zhangSan"
127.0.0.1:6379[2]> hincrby  myHash id
(error) ERR wrong number of arguments for 'hincrby' command
127.0.0.1:6379[2]> hincrby  myHash id 10
(integer) 13
127.0.0.1:6379[2]> hgetall myHash
1) "id"
2) "13"
3) "name"
4) "zhangSan"
127.0.0.1:6379[2]> hincrby  myHash id -4
(integer) 9
127.0.0.1:6379[2]> hgetall myHash
1) "id"
2) "9"
3) "name"
4) "zhangSan"
127.0.0.1:6379[2]> hincrby  myHash name -4
(error) ERR hash value is not an integer
127.0.0.1:6379[2]> keys *
1) "myHash"
127.0.0.1:6379[2]> hgetall myHash
1) "id"
2) "9"
3) "name"
4) "zhangSan"
127.0.0.1:6379[2]> hdel myHash name age
(integer) 1
127.0.0.1:6379[2]> hgetall myHash
1) "id"
2) "9"
127.0.0.1:6379[2]> hgetall myHash
1) "id"
2) "9"
127.0.0.1:6379[2]> keys *
1) "myHash"
127.0.0.1:6379[2]> hgetall myHash
1) "id"
2) "9"
127.0.0.1:6379[2]>