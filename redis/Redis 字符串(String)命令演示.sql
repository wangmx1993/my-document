
127.0.0.1:6379[2]> keys *
(empty list or set)
127.0.0.1:6379[2]> set name zhangSan
OK
127.0.0.1:6379[2]> get name
"zhangSan"
127.0.0.1:6379[2]> set name liSi
OK
127.0.0.1:6379[2]> get name
"liSi"
127.0.0.1:6379[2]> get address
(nil)
127.0.0.1:6379[2]> getset name wangWu
"liSi"
127.0.0.1:6379[2]> get name
"wangWu"
127.0.0.1:6379[2]> getset address maLiu
(nil)
127.0.0.1:6379[2]> get address
"maLiu"
127.0.0.1:6379[2]> getrange name 0 2
"wan"
127.0.0.1:6379[2]> getrange name 2 6
"ngWu"
127.0.0.1:6379[2]>  getrange name 2 10
"ngWu"
127.0.0.1:6379[2]> getrange name -3 5
"gWu"
127.0.0.1:6379[2]> getrange name -3 -1
"gWu"
127.0.0.1:6379[2]> keys *
1) "address"
2) "name"
127.0.0.1:6379[2]> mget name address
1) "wangWu"
2) "maLiu"
127.0.0.1:6379[2]> setnx name liSi
(integer) 0
127.0.0.1:6379[2]> setnx name_2 liSi
(integer) 1
127.0.0.1:6379[2]> keys *
1) "name_2"
2) "address"
3) "name"
127.0.0.1:6379[2]>  get name_2
"liSi"
127.0.0.1:6379[2]> strlen name_2
(integer) 4
127.0.0.1:6379[2]> strlen name
(integer) 6
127.0.0.1:6379[2]> set age 33
OK
127.0.0.1:6379[2]> get age
"33"
127.0.0.1:6379[2]> incr age
(integer) 34
127.0.0.1:6379[2]> incr age
(integer) 35
127.0.0.1:6379[2]> get age
"35"
127.0.0.1:6379[2]> get name
"wangWu"
127.0.0.1:6379[2]> incr name
(error) ERR value is not an integer or out of range
127.0.0.1:6379[2]> decr age
(integer) 34
127.0.0.1:6379[2]> decr age
(integer) 33
127.0.0.1:6379[2]>  decrby age 10
(integer) 23
127.0.0.1:6379[2]> incrby age 15
(integer) 38
127.0.0.1:6379[2]> get age
"38"
127.0.0.1:6379[2]> decrby name
(error) ERR wrong number of arguments for 'decrby' command
127.0.0.1:6379[2]> incrbyfloat age 15.5
"53.5"
127.0.0.1:6379[2]> incrbyfloat age 10.5
"64"
127.0.0.1:6379[2]> get name
"wangWu"
127.0.0.1:6379[2]> append name _9527
(integer) 11
127.0.0.1:6379[2]> get name
"wangWu_9527"
127.0.0.1:6379[2]> append name33 _9527
(integer) 5
127.0.0.1:6379[2]>  keys *
1) "address"
2) "age"
3) "name33"
4) "name_2"
5) "name"
127.0.0.1:6379[2]> get name33
"_9527"
127.0.0.1:6379[2]>