
127.0.0.1:6379> select 2
OK
127.0.0.1:6379[2]> keys *
(empty list or set)
127.0.0.1:6379[2]> set url www.redis.net.cn
OK
127.0.0.1:6379[2]> get url
"www.redis.net.cn"
127.0.0.1:6379[2]> hmset user_1 id 1000 name zhangSan age 25 address ￉￮ￛￚ
OK
127.0.0.1:6379[2]> hgetall user_1
1) "id"
2) "1000"
3) "name"
4) "zhangSan"
5) "age"
6) "25"
7) "address"
8) "\xc9\xee\xdb\xda"
127.0.0.1:6379[2]> lpush address shenZhen beiJing shangHai
(integer) 3
127.0.0.1:6379[2]> lpush address hangZhou
(integer) 4
127.0.0.1:6379[2]> lrange address 0 8
1) "hangZhou"
2) "shangHai"
3) "beiJing"
4) "shenZhen"
127.0.0.1:6379[2]> sadd my_set a b c 1
(integer) 4
127.0.0.1:6379[2]> sadd my_set c 1 2 3
(integer) 2
127.0.0.1:6379[2]> smembers my_set
1) "c"
2) "1"
3) "2"
4) "3"
5) "a"
6) "b"
127.0.0.1:6379[2]> zadd z_set 10 a 20 b 5 c
(integer) 3
127.0.0.1:6379[2]> zadd z_set 30 a 20 d
(integer) 1
127.0.0.1:6379[2]> zrangebyscore z_set 0 100
1) "c"
2) "b"
3) "d"
4) "a"
127.0.0.1:6379[2]> keys *
1) "address"
2) "user_1"
3) "my_set"
4) "url"
5) "z_set"
127.0.0.1:6379[2]> type z_set
zset
127.0.0.1:6379[2]> type address
list
127.0.0.1:6379[2]> ttl address
(integer) -1
127.0.0.1:6379[2]> exists url
(integer) 1
127.0.0.1:6379[2]> exists urlxxx
(integer) 0
127.0.0.1:6379[2]> expire url 10
(integer) 1
127.0.0.1:6379[2]> ttl url
(integer) 7
127.0.0.1:6379[2]> ttl url
(integer) 2
127.0.0.1:6379[2]> keys *
1) "address"
2) "user_1"
3) "my_set"
4) "z_set"
127.0.0.1:6379[2]> del z_set
(integer) 1
127.0.0.1:6379[2]> keys *
1) "address"
2) "user_1"
3) "my_set"
127.0.0.1:6379[2]>