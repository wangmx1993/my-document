---
title: zookeeper & kafka 虚拟机环境运行命令
created: '2021-12-31T12:13:37.190Z'
modified: '2022-01-04T02:38:49.143Z'
---

# zookeeper & kafka 虚拟机环境运行命令

# 启动 zk 服务器
sh /home/apache-zookeeper-3.6.1-bin/bin/zkServer.sh start
# 查看 zk 服务器状态
sh /home/apache-zookeeper-3.6.1-bin/bin/zkServer.sh status
# 关闭 zk 服务器
sh /home/apache-zookeeper-3.6.1-bin/bin/zkServer.sh stop


# 启动 kafka 服务器
nohup sh /home/kafka_2.13-2.6.0/bin/kafka-server-start.sh > /home/kafka_2.13-2.6.0/logs/server.log /home/kafka_2.13-2.6.0/config/server.properties &
# 停止 kafka 服务器
/home/kafka_2.13-2.6.0/bin/kafka-server-stop.sh

# 查看 java 进程
jps

# 查看 kafka 所有的主题
/home/kafka_2.13-2.6.0/bin/kafka-topics.sh --list --bootstrap-server 192.168.116.128:9092,192.168.116.129:9092

# 查看 kafka 具体主题的详细信息
/home/kafka_2.13-2.6.0/bin/kafka-topics.sh --describe --bootstrap-server 192.168.116.128:9092,192.168.116.129:9092 -topic helloWorld

# 启动生产者发送消息
/home/kafka_2.13-2.6.0/bin/kafka-console-producer.sh --bootstrap-server 192.168.116.128:9092,192.168.116.129:9092 --topic helloWorld

# 创建主题
/home/kafka_2.13-2.6.0/bin/kafka-topics.sh --create --bootstrap-server 192.168.116.128:9092,192.168.116.129:9092 --replication-factor 2 --partitions 2 --topic helloWorld

# 启动消费者接收消息
/home/kafka_2.13-2.6.0/bin/kafka-console-consumer.sh --bootstrap-server 192.168.116.128:9092,192.168.116.129:9092 --from-beginning --topic hello-world


# 启动 Rocket NameServer 名称服务器
nohup sh /home/rocketmq-4.7.1/distribution/target/rocketmq-4.7.1/rocketmq-4.7.1/bin/mqnamesrv &
#查看 Rocket NameServer 名称服务器 日志
tail -f ~/logs/rocketmqlogs/namesrv.log

# 启动 Rocket Broker 服务节点
nohup sh /home/rocketmq-4.7.1/distribution/target/rocketmq-4.7.1/rocketmq-4.7.1/bin/mqbroker -n localhost:9876 &
# 查看 Broker 日志
tail -f ~/logs/rocketmqlogs/broker.log


systemctl stop firewalld               #关闭防火墙
systemctl status firewalld             #查看防火墙状态

# 查看指定端口的运行情况
netstat -ano | grep 6379

firewall-cmd --zone=public --list-ports



