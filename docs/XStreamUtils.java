

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.Dom4JDriver;
import com.thoughtworks.xstream.io.xml.DomDriver;

import java.io.*;
import java.nio.charset.Charset;
import java.util.*;

/**
 * Created by Administrator on 2019/1/16 0016.
 */
public class XStreamUtils {
    /**
     * POJO 对象转 XML 字符串
     * 方法运行后输出示例如下：
     * POJO:
     * User{birthday=Thu Jan 17 11:48:15 CST 2019, uId=101, uName='Joni', salary=8989.88, info='null'}
     * xml:
     * <com.lct.test.User>
     * <uId>101</uId>
     * <uName>Joni</uName>
     * <birthday>2019-01-17 03:48:15.36 UTC</birthday>
     * <salary>8989.88</salary>
     * </com.lct.test.User>
     */
    public static final void POJO2XmlString() {
        User user = new User();
        user.setuId(101);
        user.setuName("Joni");
        user.setBirthday(new Date());
        user.setSalary(8989.88f);

        XStream xStream = new XStream();
        String userXml = xStream.toXML(user);
        System.out.println("POJO:\n" + user);
        System.out.println("xml:\n" + userXml);
    }

    /**
     * POJO 对象转 XML 字符串，使用别名修改包名
     * 方法运行后输出如下：
     * user:
     * User{birthday=Thu Jan 17 11:50:34 CST 2019, uId=101, uName='Joni', salary=8989.88, info='null'}
     * userXml:
     * <pojo.User>
     * <uId>101</uId>
     * <uName>Joni</uName>
     * <birthday>2019-01-17 03:47:07.146 UTC</birthday>
     * <salary>8989.88</salary>
     * </pojo.User>
     */
    public static final void POJO2XmlStringAliasPackage() {
        User user = new User();
        user.setuId(101);
        user.setuName("Joni");
        user.setBirthday(new Date());
        user.setSalary(8989.88f);

        XStream xStream = new XStream();
        /**className： user 的类名，如 com.lct.test.User;
         * packageName：user 类的包名，com.lct.test*/
        String className = user.getClass().getName();
        String packageName = className.substring(0, className.lastIndexOf("."));
        /**使用 pojo 替换默认的包名 com.lct.test*/
        xStream.aliasPackage("pojo", packageName);

        String userXml = xStream.toXML(user);
        System.out.println("user:\n" + user);
        System.out.println("userXml:\n" + userXml);
    }

    /**
     * POJO 对象转为 XML字符串，使用别名修改 POJO 类名
     * 运行输出示例如下：
     * user:
     * User{birthday=Thu Jan 17 11:56:15 CST 2019, uId=101, uName='Joni', salary=8989.88, info='null'}
     * userXml:
     * <user>
     * <uId>101</uId>
     * <uName>Joni</uName>
     * <birthday>2019-01-17 03:56:15.498 UTC</birthday>
     * <salary>8989.88</salary>
     * </user>
     */
    public static final void POJO2XmlStringAliasClass() {
        User user = new User();
        user.setuId(101);
        user.setuName("Joni");
        user.setBirthday(new Date());
        user.setSalary(8989.88f);

        XStream xStream = new XStream();
        /**使用别名修改默认的类名*/
        xStream.alias("user", User.class);

        //xml：将 POJO 对象转为xml字符串
        String userXml = xStream.toXML(user);
        System.out.println("user:\n" + user);
        System.out.println("userXml:\n" + userXml);
    }

    /**
     * POJO转为XML字符串2-----使用别名修改POJO属性名
     * 运行输出如下：
     * user:
     * User{birthday=Thu Jan 17 12:28:11 CST 2019, uId=101, uName='Joni', salary=8989.88, info='null'}
     * userXml:
     * <user>
     * <userId>101</userId>
     * <userName>Joni</userName>
     * <birthday>2019-01-17 04:28:11.551 UTC</birthday>
     * <salary>8989.88</salary>
     * </user>
     */
    public static final void POJO2XmlStringAliasField() {
        User user = new User();
        user.setuId(101);
        user.setuName("Joni");
        user.setBirthday(new Date());
        user.setSalary(8989.88f);

        XStream xStream = new XStream();
        /**使用别名修改默认的类名*/
        xStream.alias("user", User.class);
        /**
         * 使用别名修改类的属性名称
         * aliasField(String alias, Class definedIn, String fieldName)
         * alias：别名、definedIn：类型、fieldName：被修改的字段名
         */
        xStream.aliasField("userId", User.class, "uId");
        xStream.aliasField("userName", User.class, "uName");

        //xml：将 POJO 对象转为xml字符串
        String userXml = xStream.toXML(user);
        System.out.println("user:\n" + user);
        System.out.println("userXml:\n" + userXml);
    }

    /**
     * POJO List 转 XML 字符串
     */
    public static final void POJOList2XmlString() {
        User user1 = new User();
        User user2 = new User();
        user1.setuId(1000);
        user1.setuName("zhangSan");
        user1.setBirthday(new Date());
        user1.setSalary(8989.88f);
        user2.setuId(1002);
        user2.setuName("LiSi");
        user2.setBirthday(new Date());
        user2.setSalary(12000.00f);

        List<User> userList = new ArrayList<User>();
        userList.add(user1);
        userList.add(user2);

        XStream xStream = new XStream();
        /**可以选择为根标签的设置别名
         * xStream.alias("user", User.class);
         * xStream.alias("userList", List.class);
         * */
        String userXml = xStream.toXML(userList);
        System.out.println(userXml);
    }

    /**
     * 将POJO Array转为XML字符串
     */
    public static final void POJOArray2XmlString() {
        User user1 = new User();
        User user2 = new User();
        user1.setuId(1000);
        user1.setuName("Joni");
        user1.setBirthday(new Date());
        user1.setSalary(9800.88f);
        user2.setuId(1001);
        user2.setuName("Tom");
        user2.setBirthday(new Date());
        user2.setSalary(14000.00f);

        User[] userArray = new User[2];
        userArray[0] = user1;
        userArray[1] = user2;

        XStream xStream = new XStream();
        /**可以选择为根标签的设置别名
         * xStream.alias("user", User.class);
         * xStream.alias("userList", List.class);
         * */
        String userXml = xStream.toXML(userArray);
        System.out.println(userXml);
    }

    /**
     * 将POJO Map转为XML字符串
     */
    public static final void POJOMap2XmlString() {
        User user1 = new User();
        User user2 = new User();
        user1.setuId(9526);
        user1.setuName("Joni");
        user1.setBirthday(new Date());
        user1.setSalary(9800.88f);
        user2.setuId(9527);
        user2.setuName("Tom");
        user2.setBirthday(new Date());
        user2.setSalary(14000.00f);

        Map<String, User> userMap = new Hashtable<String, User>();
        userMap.put("user1", user1);
        userMap.put("user2", user2);

        XStream xStream = new XStream();
        /**也可以为 User 类与 Hashtable 类设置别名
         * xStream.alias("user", User.class);
         * xStream.alias("userMap", Hashtable.class);
         */
        String userXml = xStream.toXML(userMap);
        System.out.println(userXml);
    }


    /**
     * 将单个的 POJO 对象转换为 xml 字符串并写入到文件中
     */
    public static final void POJO2XmlWriteDisk() {
        try {
            User user = new User();
            user.setuId(101);
            user.setuName("Joni");
            user.setBirthday(new Date());
            user.setSalary(8989.88f);
            user.setInfo("测试用户");

            /**理论上自然是可以保存为任意类型的文件，但是既然是解析xml，则保存为 xml 格式即可
             * saveFilePath：xml文件的路径，*.xml文件不存在时会自动创建，但是其父目录必须存在，即如下所示的 E:/wmx/test/ 必须存在，
             * 否则报错 "java.io.FileNotFoundException",xml文件存在时内容会覆盖
             * 这里使用 PrintWriter-字符打印输出流，它继承于Writer。
             * */
            String saveFilePath = "E:/wmx/test/" + new Random().nextInt(10000) + ".xml";
            PrintWriter printWriter = new PrintWriter(saveFilePath, "UTF-8");
            /**标准的xml文件是必须得有根标签 <xml> 的，根据实际需求绝对是否添加
             * 当不加 <xml>根标签时，使用 XStream 能快速的反序列化，将文件内容直接转换为 POJO、Lsit 等*/
            printWriter.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");

            XStream xStream = new XStream(new DomDriver());
            /**toXML(Object obj, Writer out)
             * obj 也可以 List、map、array 等*/
            xStream.toXML(user, printWriter);
            printWriter.flush();
            printWriter.close();
            System.out.println("xml 文件生成完毕...");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    /**
     * 将多个 POJO 对象转换为 xml 字符串并写入到文件中
     */
    public static final void POJOs2XmlWriteDisk() {
        try {
            User user1 = new User();
            User user2 = new User();
            user1.setuId(1000);
            user1.setuName("张三");
            user1.setBirthday(new Date());
            user1.setSalary(8989.88f);
            user2.setuId(1200);
            user2.setuName("李思思");
            user2.setBirthday(new Date());
            user2.setSalary(12000.00f);
            List<User> userList = new ArrayList<User>();
            userList.add(user1);
            userList.add(user2);

            /**理论上自然是可以保存为任意类型的文件，但是既然是解析xml，则保存为 xml 格式即可
             * saveFilePath：xml文件的路径，*.xml文件不存在时会自动创建，但是其父目录必须存在，即如下所示的 E:/wmx/test/ 必须存在，
             * 否则报错 "java.io.FileNotFoundException",xml文件存在时内容会覆盖。这里使用 OutputStream-字节输出流
             * */
            String saveFilePath = "E:/xml/" + new Random().nextInt(10000) + ".xml";
            OutputStream outputStream = new FileOutputStream(saveFilePath);
            XStream xStream = new XStream();
            /**toXML(Object obj, OutputStream out)
             * obj 也可以 List、map、array 等；out：字节输出流*/
            xStream.toXML(userList, outputStream);
            outputStream.flush();
            outputStream.close();
            System.out.println("xml 文件生成完毕...");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 读取 xml 文件中的内容转为 POJO对象列表
     *
     * @param hotWordFile : xml文件对象
     */
    public static final void xml2POJOs() {
        /**
         * DomDriver：纯 Java JDK Dom 解析驱动，此时无需导入额外的 Jar 包，只要导入 xstream、xmlpill两个 jar 包即可
         * Dom4JDriver：dom4j 解析，此时需要额外导入 dom4j-xxx.jar 包
         * XppDriver、XppDomDriver、Xpp3Driver、Xpp3DomDriver：xpp 解析驱动，此时需要额外导入 xpp3_min-xxx.jar 包
         * 使用无参默认构造器 XStream()时，默认会使用 XppDriver 解析驱动，所以需要导入 xpp3 包，否则抛异常
         * 反序列化使用的驱动与序列化时使用的驱动无关，不用一致
         **/
        XStream xStream = new XStream();
        /**如果 toXML 的时候使用了别名，则反序列化的时候也要使用别名*/
        xStream.alias("user", User.class);

        /**fromXML(Reader reader)：reader 是字符输入流，有很多子类，
         * 可以通过 InputStreamReader 将字节流转为字符流，也可以直接使用 FileReader
         * 如：Reader reader = new FileReader("E:/xml/8017.xml");
         * 但是不能用 StringReader ,推荐 FileReader
         * */
        InputStream inputStream = null;
        try {
            inputStream = new FileInputStream("E:/xml/8017.xml");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        Reader reader = new InputStreamReader(inputStream, Charset.forName("UTF-8"));
        /**fromXML 返回的 Object ，如果是单个对象，则转为 POJO对象，如果是多个，则转为list即可
         * */
        List<User> userList = (List<User>) xStream.fromXML(reader);
        for (User user : userList) {
            System.out.println("user:" + user);
        }
        try {
            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
    }
}
