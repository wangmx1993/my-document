
-- 建表
DROP TABLE IF EXISTS `person2`;
CREATE TABLE `person2` (
  `pid` int(11) NOT NULL AUTO_INCREMENT,
  `pname` varchar(16) NOT NULL,
  `page` int(8) DEFAULT NULL,
  `info` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`pid`),
  KEY `index_pname` (`pname`)
) ENGINE=InnoDB AUTO_INCREMENT=775279 DEFAULT CHARSET=utf8;



-- 使用存储过程插入 100 万条数据

drop procedure if exists insert_person2; -- 如果存在指定的存储过程，则删除
#将sql语句的结束符由默认的分号";"临时改为"$$"(可以自定义符号)，这样里面才能正常使用分号";"，否则会被当做结束符处理
delimiter $$
create procedure insert_person2() # 声明存储过程，即使没有参数，也得有括号
begin  # 开始
	declare v_index INT default 984454;  
	WHILE v_index <= 1000000 DO
	INSERT INTO person2(pname,page,info) VALUES(CONCAT('用户',v_index),v_index % 50 + 3,'已婚'); -- pid 主键自增，所以不用管
	SET v_index = v_index + 1;
	end WHILE;
end $$ # 结束
delimiter ;  #将语句的结束符号恢复为分号
 


call insert_person2(); -- 调用存储过程