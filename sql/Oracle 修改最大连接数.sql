Microsoft Windows [版本 10.0.18362.476]
(c) 2019 Microsoft Corporation。保留所有权利。

//hnbs_3/1 是登陆用户与密码，以 sysdba 连接
C:\Users\Think>sqlplus hnbs_3/1 as sysdba
SQL*Plus: Release 11.2.0.1.0 Production on 星期一 11月 30 14:12:42 2020
Copyright (c) 1982, 2010, Oracle.  All rights reserved.
已连接到空闲例程。

//当无法正常启动时，以 pfile 形式启动
SQL> startup pfile='C:\app\Think\admin\orcl\pfile\init.ora.1123201992052'
ORACLE 例程已经启动。

Total System Global Area 3390558208 bytes
Fixed Size                  2180464 bytes
Variable Size            1862273680 bytes
Database Buffers         1509949440 bytes
Redo Buffers               16154624 bytes
数据库装载完毕。
数据库已经打开。
SQL> create spfile from pfile;
create spfile from pfile
*
第 1 行出现错误:
ORA-01078: 处理系统参数失败
LRM-00109: could not open parameter file
'C:\APP\THINK\PRODUCT\11.2.0\DBHOME_1\DATABASE\INITORCL.ORA'

SQL> create spfile from pfile='C:\app\Think\admin\orcl\pfile\init.ora.1123201992052';

文件已创建。

SQL> shutdown immediate;
数据库已经关闭。
已经卸载数据库。
ORACLE 例程已经关闭。
SQL> startup
ORACLE 例程已经启动。

Total System Global Area 3390558208 bytes
Fixed Size                  2180464 bytes
Variable Size            1862273680 bytes
Database Buffers         1509949440 bytes
Redo Buffers               16154624 bytes
数据库装载完毕。
数据库已经打开。

//查看最大连接数
SQL> show parameter processes;
NAME                                 TYPE        VALUE
------------------------------------ ----------- ------------------------------
aq_tm_processes                      integer     0
db_writer_processes                  integer     1
gcs_server_processes                 integer     0
global_txn_processes                 integer     1
job_queue_processes                  integer     1000
log_archive_max_processes            integer     4
processes                            integer     150

//设置最大连接数
SQL> alter system set processes=500 scope=spfile;
系统已更改。
SQL> alter system set sessions=555 scope=spfile;
系统已更改。

SQL> shutdown immediate;
数据库已经关闭。
已经卸载数据库。
ORACLE 例程已经关闭。
SQL> startup
ORACLE 例程已经启动。

Total System Global Area 3390558208 bytes
Fixed Size                  2180464 bytes
Variable Size            1862273680 bytes
Database Buffers         1509949440 bytes
Redo Buffers               16154624 bytes
数据库装载完毕。
数据库已经打开。

SQL> show parameter processes;
NAME                                 TYPE        VALUE
------------------------------------ ----------- ------------------------------
aq_tm_processes                      integer     0
db_writer_processes                  integer     1
gcs_server_processes                 integer     0
global_txn_processes                 integer     1
job_queue_processes                  integer     1000
log_archive_max_processes            integer     4
processes                            integer     500
SQL> show parameter sessions;

NAME                                 TYPE        VALUE
------------------------------------ ----------- ------------------------------
java_max_sessionspace_size           integer     0
java_soft_sessionspace_limit         integer     0
license_max_sessions                 integer     0
license_sessions_warning             integer     0
sessions                             integer     776
shared_server_sessions               integer
SQL>

