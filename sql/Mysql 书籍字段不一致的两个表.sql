
-- 数据准备(故意让 book 与 book_2 的 title，info 字段位置不对应)
DROP TABLE IF EXISTS `book`;
CREATE TABLE `book` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `title` varchar(128) NOT NULL COMMENT '标题',
  `price` float(8,2) DEFAULT NULL COMMENT '价格',
  `publish` date DEFAULT NULL COMMENT '发布时间',
  `info` varchar(128) DEFAULT NULL COMMENT '描述',
  `type_id` int(11) DEFAULT NULL COMMENT '外键',
  PRIMARY KEY (`id`),
  KEY `type_id` (`type_id`),
  CONSTRAINT `book_ibfk_1` FOREIGN KEY (`type_id`) REFERENCES `book_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
 
-- ----------------------------
INSERT INTO `book` VALUES ('1', '三国演义', '88.68', '2019-09-08', '', '5');
INSERT INTO `book` VALUES ('2', '红楼梦', '48.35', '2019-03-08', '一个家族的兴衰', '3');
INSERT INTO `book` VALUES ('3', '青云志', '58.48', '2019-05-08', '少年修仙传奇', '4');
INSERT INTO `book` VALUES ('4', '三体', '83.68', '2019-04-08', '', '2');
INSERT INTO `book` VALUES ('5', '笑傲江湖', '81.18', '2019-06-08', '金庸武侠', '1');
INSERT INTO `book` VALUES ('6', '倚天屠龙记', '45.78', '2019-07-08', '金庸武侠', '1');
INSERT INTO `book` VALUES ('7', '中华上下五千年', '65.35', '2019-01-08', null, null);
 
DROP TABLE IF EXISTS `book_2`;
CREATE TABLE `book_2` (
  `id` int(11) NOT NULL DEFAULT '0',
  `info` varchar(128) DEFAULT NULL COMMENT '描述',
  `price` float(8,2) DEFAULT NULL COMMENT '价格',
  `publish` date DEFAULT NULL COMMENT '发布时间',
  `title` varchar(128) NOT NULL COMMENT '标题',
  `type_id` int(11) DEFAULT NULL COMMENT '外键',
  PRIMARY KEY (`id`),
  KEY `type_id` (`type_id`),
  CONSTRAINT `book_ibfk_2` FOREIGN KEY (`type_id`) REFERENCES `book_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;