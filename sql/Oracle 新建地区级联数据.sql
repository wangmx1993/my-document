
--菜单目录结构表
create table scott_menu(
    id number(10) primary key, --主键id
    title varchar2(50), --菜单名称
    menu_level number(1),--菜单层级，如 1 表示 1 级菜单,2表示2级菜单，以此类推
    parentId number(10) --父菜单 id
);
 
--添加1级菜单
insert into scott_menu(id, title, menu_level, parentId) values(1, '湖南省',1,0);--顶级菜单没有父菜单，所以 parentId 为 0
insert into scott_menu(id, title, menu_level, parentId) values(2, '广东省',1,0);
 
 
--添加2级菜单
insert into scott_menu(id, title, menu_level, parentId) values(3, '长沙市',2,1);
insert into scott_menu(id, title, menu_level, parentId) values(4, '娄底市',2,1);
insert into scott_menu(id, title, menu_level, parentId) values(5, '深圳市',2,2);
 
--添加3级菜单
insert into scott_menu(id, title, menu_level, parentId) values(6, '芙蓉区',3,3);
insert into scott_menu(id, title, menu_level, parentId) values(7, '天心区',3,3);
insert into scott_menu(id, title, menu_level, parentId) values(8, '岳麓区',3,3);
insert into scott_menu(id, title, menu_level, parentId) values(9, '开福区',3,3);
insert into scott_menu(id, title, menu_level, parentId) values(10, '雨花区',3,3);
insert into scott_menu(id, title, menu_level, parentId) values(11, '望城区',3,3);
 
insert into scott_menu(id, title, menu_level, parentId) values(12, '娄星区',3,4);   
insert into scott_menu(id, title, menu_level, parentId) values(13, '冷水江市',3,4); 
insert into scott_menu(id, title, menu_level, parentId) values(14, '涟源市',3,4); 
insert into scott_menu(id, title, menu_level, parentId) values(15, '双峰县',3,4);
insert into scott_menu(id, title, menu_level, parentId) values(16, '新化县',3,4);
 
insert into scott_menu(id, title, menu_level, parentId) values(17, '罗湖区',3,5);      
insert into scott_menu(id, title, menu_level, parentId) values(18, '福田区',3,5);
insert into scott_menu(id, title, menu_level, parentId) values(19, '南山区',3,5);
insert into scott_menu(id, title, menu_level, parentId) values(20, '宝安区',3,5);
insert into scott_menu(id, title, menu_level, parentId) values(21, '坪山区',3,5);
insert into scott_menu(id, title, menu_level, parentId) values(22, '龙岗区',3,5);  
 
--添加4级菜单 
insert into scott_menu(id, title, menu_level, parentId) values(23, '白溪镇',4,16);  
insert into scott_menu(id, title, menu_level, parentId) values(24, '洋溪镇',4,16);  
insert into scott_menu(id, title, menu_level, parentId) values(25, '吉庆镇',4,16);  
insert into scott_menu(id, title, menu_level, parentId) values(26, '曹家镇',4,16);  